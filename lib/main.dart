import 'package:flutter/material.dart';
import 'dart:math';

void main() {
  return runApp(
    MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.indigo[700],
        appBar: AppBar(
          title: Text('Ask ball anything'),
          backgroundColor: Colors.indigo[900],
        ),
        body: MyBody(),
      ),
    ),
  );
}

class MyBody extends StatefulWidget {
  @override
  _DicePageState createState() => _DicePageState();
}

class _DicePageState extends State<MyBody> {
  int ballNumber = 0;

  void askBall() {
    setState(() {
      ballNumber = Random().nextInt(5) + 1;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        child: (FlatButton(
          onPressed: () {
            askBall();
          },
          child: Image.asset('images/ball$ballNumber.png'),
        )),
      ),
    );
  }
}
